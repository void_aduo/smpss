<?php
class exe_sql {
	public $db;
	public $err;
	
	function __construct($database, $db) {
		$this->db = $db;
		if (! mysqli_select_db ($this->db, $database)) {
			//$database = "CREATE DATABASE {$database}";
			//if (! mysql_query ( $database, $this->db )) {
				die ( "无法选择数据库，请检查是否已经创建数据库：{$database}！" );
			//}
			//mysql_select_db ( $database, $this->db );
		}
		$result = mysqli_query ($this->db, "set names 'utf8'" );
	}
	function seterr($msg) {
		$this->err = $msg;
	}
	function geterr() {
		return $this->err;
	}
	function __destruct() {
		mysqli_close ( $this->db );
	}
	function run($sqls,$admin='') {
		if (! is_array ( $sqls )) {
			$this->seterr ( "SQL 为空！" );
			return false;
		}
		foreach ( $sqls as $sql ) {
			$items = $this->parse_sql_file ( $sql );
			if (! $items) {
				continue;
			}
			foreach ( $items as $item ) {
				if (! $item) {
					continue;
				}
				if (! mysqli_query ($this->db, $item)) {
					$this->seterr ( "SQL查询错误！".mysqli_error($this->db) );
					return false;
				}
			}
		}
		if($admin){
			 mysqli_query ($this->db, "delete from smpss_admin" );//删除默认的帐号
			 mysqli_query ($this->db, $admin);//创建新管理员帐号
		}
		return true;
	}
	
	function parse_sql_file($file_path) {
		if (! file_exists ( $file_path )) {
			$this->seterr ( "指定的数据库文件不存在！" );
			return false;
		}
		$sql = implode ( '', file ( $file_path ) );
		$sql = preg_replace ( '/^\s*(?:--|#).*/m', '', $sql );
		$sql = preg_replace ( '/^\s*\/\*.*?\*\//ms', '', $sql );
		$sql = trim ( $sql );
		if (! $sql) {
			$this->seterr ( "没有SQL已经！" );
			return false;
		}
		$sql = str_replace ( "\r", '', $sql );
		return explode ( ";\n", $sql );
	}
}
?>